import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';
import {notifikasi} from './Notifikasi';

export class index extends Component {
  klikTombol = () => {
    notifikasi.configure();
    notifikasi.buatChannel('1');
    notifikasi.kirimNotifikasi(
      '1',
      'Berhasil Menyimpan Buku ke Favorit!',
      'Buku Harry Potter and the Order of the Phoenix berhasil ditambahkan ke Favorit!',
    );
  };
  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
        <Button title="notifikasi" onPress={this.klikTombol}></Button>
      </View>
    );
  }
}

export default index;
