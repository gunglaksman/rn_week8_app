import React, {Component} from 'react';
import {Text, TextInput, TouchableOpacity, View, Image} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ui from '../../login';
import axios from 'axios';

export class index extends Component {
  constructor() {
    super();
    this.state = {
      isiEmail: 'agunable@gmail.com',
      isiPassword: 'Blackteen2',
    };
  }

  handleSubmit = async () => {
    try {
      const hasil = await axios.post(
        'http://code.aldipee.com/api/v1/auth/login',
        {
          email: this.state.isiEmail,
          password: this.state.isiPassword,
        },
      );
      console.log(hasil.data.tokens.access.token);
      await AsyncStorage.setItem('token', hasil.data.tokens.access.token);
      this.props.navigation.navigate('Home');
    } catch (error) {
      console.log(error);
    }
  };

  handleChangeEmail = event => {
    this.setState({
      isiEmail: event,
    });
  };

  handleChangePassword = event => {
    this.setState({
      isiPassword: event,
    });
  };

  render() {
    return (
      <View style={ui.container}>
        <View style={ui.header}>
          <Image
            style={{width: 150, height: 150}}
            source={require('../../../assets/logo.png')}
          />
        </View>
        <View style={ui.input}>
          <Text style={ui.judul}> Sign In</Text>
          <Text style={ui.textInput}>Email:</Text>
          <TextInput
            placeholder="Isi dengan Email kamu.."
            value={this.state.isiEmail}
            onChangeText={this.handleChangeEmail}
            style={ui.kotakInputEmail}
          />
          <Text style={ui.textInput}>Password:</Text>
          <TextInput
            placeholder="Isi dengan Password kamu.."
            value={this.state.isiPassword}
            onChangeText={this.handleChangePassword}
            secureTextEntry={true}
            style={ui.kotakInputEmail}
          />
          <TouchableOpacity onPress={this.handleSubmit} style={ui.createButton}>
            <Text style={ui.textCreate}>Sign In</Text>
          </TouchableOpacity>
          <View
            style={{
              marginTop: 5,
              flex: 1,
              flexDirection: 'row',
            }}>
            <Text style={ui.textInput}>Don't have an account?</Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Signup', {
                  nama: '',
                });
              }}>
              <Text style={ui.login}> Sign up</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default index;
