import React, {Component} from 'react';
import {
  Button,
  Text,
  View,
  BackHandler,
  ScrollView,
  ImageBackground,
  Image,
  TouchableOpacity,
  StatusBar,
  RefreshControl,
} from 'react-native';
import ui from '../../detail';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {
  faChevronCircleLeft,
  faShareAlt,
  faHeart,
  faChevronLeft,
} from '@fortawesome/free-solid-svg-icons';
import Share from 'react-native-share';
import axios from 'axios';
import {warna} from '../../color';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {notifikasi} from '../test/Notifikasi';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      detailMovie: {},
      refreshing: false,
      contohState: 1,
    };
  }

  componentDidMount = async () => {
    console.log(this.props.route.params.idFilm);
    StatusBar.setBackgroundColor(warna.primary);
    const token = await AsyncStorage.getItem('token');
    const hasil = await axios.get(
      `http://code.aldipee.com/api/v1/books/${this.props.route.params.idFilm}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    this.setState({
      detailMovie: hasil.data,
    });
  };

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.detailMovie !== this.state.detailMovie;
  }
  klikTombol = () => {
    notifikasi.configure();
    notifikasi.buatChannel('1');
    notifikasi.kirimNotifikasi(
      '1',
      'Berhasil Menyimpan Buku ke Favorit!',
      `Buku ${this.state.detailMovie.title} berhasil ditambahkan ke Favorit!`,
    );
  };
  votingStar(vote) {
    if (vote >= 5 && vote < 7) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    } else if (vote >= 7 && vote < 9) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    } else if (vote >= 9 && vote <= 10) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
        </>
      );
    } else {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    }
  }
  onRefresh = async () => {
    this.setState({refreshing: true});
    const hasil = await axios.get(
      `http://code.aldipee.com/api/v1/movies/${this.props.route.params.idFilm}`,
    );
    this.setState({
      detailMovie: hasil.data,
      refreshing: false,
      contohState: 2,
    });
  };

  handleBackButtonClick = () => {
    this.props.navigation.goBack();
    return true;
  };

  socShare = async () => {
    const shareOptions = {
      title: 'Share via',
      message: `Yuk nonton film ${this.state.detailMovie.title} dengan rating ${this.state.detailMovie.average_rating}`,
      url: `${this.state.detailMovie.cover_image}`,
      social: Share.Social.WHATSAPP,
    };
    const shareResponse = await Share.shareSingle(shareOptions);
  };
  render() {
    return (
      <View style={ui.container}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }>
          <View style={ui.header}>
            <ImageBackground
              source={{
                uri: `${this.state.detailMovie.cover_image}`,
              }}
              resizeMode="stretch"
              style={ui.imagebg}>
              <View style={ui.overlay}>
                <TouchableOpacity
                  style={{flex: 1}}
                  onPress={this.handleBackButtonClick}>
                  <FontAwesomeIcon
                    size={25}
                    color="#FFFF"
                    icon={faChevronCircleLeft}
                  />
                </TouchableOpacity>
                <TouchableOpacity onPress={this.klikTombol}>
                  <FontAwesomeIcon size={20} color="#FFFF" icon={faHeart} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{marginLeft: 10}}
                  onPress={this.socShare}>
                  <FontAwesomeIcon size={20} color="#FFFF" icon={faShareAlt} />
                </TouchableOpacity>
              </View>
            </ImageBackground>
            <Image
              style={ui.imageCard}
              source={require('../../../assets/header.png')}
            />
          </View>
          <View style={ui.description}>
            <View style={ui.containerDeskripsi}>
              <View style={{flexDirection: 'column', flex: 0.1}}>
                <Text style={ui.judulFilm}>{this.state.detailMovie.title}</Text>
                <Text style={ui.tagline}>{this.state.detailMovie.author}</Text>
                <Text style={ui.status}>
                  {this.state.detailMovie.page_count} {' Pages '}|{' '}
                  {this.state.detailMovie.publisher} | Rp.{' '}
                  {this.state.detailMovie.price}
                </Text>
              </View>
              <View
                style={{flexDirection: 'row', width: 100, flex: 1, padding: 1}}>
                <Text style={ui.starFont}>
                  {this.state.detailMovie.average_rating}
                </Text>
                {this.votingStar(this.state.detailMovie.average_rating)}
              </View>
            </View>
            <View style={ui.deskripsi}>
              <Text style={ui.fontCastJudul}>Author</Text>
              <View key={index} style={ui.viewGenre}>
                <View style={ui.cardGenre}>
                  <Text style={ui.fontCardGenre}>
                    {this.state.detailMovie.author}
                  </Text>
                </View>
              </View>
            </View>
            <View style={ui.deskripsix}>
              <Text style={ui.fontCastJudul}>Synopsis</Text>
              <Text style={ui.fontDeskripsi}>
                {this.state.detailMovie.synopsis}
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
