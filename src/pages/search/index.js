import React, {Component, useCallback} from 'react';
import {
  View,
  Text,
  StatusBar,
  ScrollView,
  ImageBackground,
  Image,
  Button,
  TouchableOpacity,
  RefreshControl,
  Alert,
  TextInput,
} from 'react-native';
import {warna} from '../../color';
import ui from '../../style';
import axios from 'axios';
import InternetConnectionAlert from 'react-native-internet-connection-alert';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {addBook} from '../../redux/actions/book';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHome, faHeart, faSearch} from '@fortawesome/free-solid-svg-icons';

class index extends Component {
  constructor() {
    super();
    this.state = {
      refreshing: false,
      timePassed: true,
      userFiltered: [],
      asearch: '',
      isiEmail: 'agunable@gmail.com',
    };
  }

  componentDidMount = async () => {
    StatusBar.setBackgroundColor(warna.primary);
    try {
      const token = await AsyncStorage.getItem('token');
      const hasil = await axios.get('http://code.aldipee.com/api/v1/books', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      this.props.add(hasil.data.results); //Dispatch ke store
      console.log(this.props.books);
    } catch (error) {
      console.log(error);
      this.props.navigation.navigate('Login');
    }
  };

  delay = ms => new Promise(res => setTimeout(res, ms));
  shouldComponentUpdate(nextProps, nextState) {
    return nextState.latestUpload !== this.state.latestUpload;
  }

  onRefresh = async () => {
    this.setState({refreshing: true});
    const hasil = await axios.get('http://code.aldipee.com/api/v1/movies/');
    this.setState({
      latestUpload: hasil.data.results,
      refreshing: false,
    });
  };

  votingStar(vote) {
    if (vote >= 5 && vote < 7) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    } else if (vote >= 7 && vote < 9) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    } else if (vote >= 9 && vote <= 10) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
        </>
      );
    } else {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    }
  }

  searchBuku = event => {
    if (event) {
      const newData = this.props.books.filter(item => {
        const itemData = item.title
          ? item.title.toLowerCase()
          : ''.toLowerCase();
        const textData = event.toLowerCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        userFiltered: newData,
        asearch: event,
      });
      console.log('masuk');
    } else {
      this.setState({
        userFiltered: this.props.books,
        asearch: event,
      });
    }

    // console.log(this.state.asearch);
  };

  render() {
    return this.state.timePassed === false ? (
      <ImageBackground
        source={require('../../../assets/splash.png')}
        style={ui.background}
      />
    ) : (
      <View style={ui.container}>
        <InternetConnectionAlert
          onChange={connectionState => {
            console.log('Connection State: ', connectionState);
          }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View style={ui.listContainer}>
              <View style={ui.searchSection}>
                <FontAwesomeIcon
                  color="#FFFF"
                  size={20}
                  icon={faSearch}
                  style={{marginLeft: 10}}
                />
                <TextInput
                  placeholder="Cari buku kamu.."
                  defaultValue={this.state.asearch}
                  placeholderTextColor="#808080"
                  onChangeText={this.searchBuku}
                  style={ui.search}
                />
              </View>

              <Text style={ui.judul}>Search Results</Text>
              <ScrollView>
                <View style={ui.listComponent}>
                  {this.props.books
                    .filter(item =>
                      item.title.toLowerCase().includes(this.state.asearch),
                    )
                    .map((item, index) => (
                      <View key={index} style={ui.listCard}>
                        <View style={ui.overlay}>
                          <Image
                            style={ui.imageCard}
                            source={{
                              uri: `${item.cover_image}`,
                            }}
                          />
                          <View style={ui.containerDeskripsi}>
                            <Text style={ui.judulFilm}>{item.title}</Text>
                            <Text style={ui.tanggal}>{item.author}</Text>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                              <View style={{flex: 0.6, flexDirection: 'row'}}>
                                {this.votingStar(item.average_rating)}
                                <Text style={ui.starFont}>
                                  {item.average_rating}
                                </Text>
                              </View>
                              <View style={{flex: 1, flexDirection: 'row'}}>
                                <Text style={ui.fontGenre}>
                                  {item.publisher}
                                </Text>
                              </View>
                            </View>
                            <TouchableOpacity
                              onPress={() => {
                                this.props.navigation.navigate('DetailMovie', {
                                  idFilm: item.id,
                                });
                              }}
                              style={ui.buttonMore}>
                              <Text style={ui.fontButton}>Show More</Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    ))}
                </View>
              </ScrollView>
            </View>
          </ScrollView>
        </InternetConnectionAlert>
      </View>
    );
  }
}

const mapStatetoProps = state => {
  console.log(state);
  return {
    books: state.bookReducer.bookList,
    favs: state.bookReducer.favList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    add: book => dispatch(addBook(book)),
  };
};

export default connect(mapStatetoProps, mapDispatchToProps)(index);
