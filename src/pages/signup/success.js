import React, {Component} from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  Button,
} from 'react-native';
import {showMessage, hideMessage} from 'react-native-flash-message';
import ui from '../../login';
import axios from 'axios';

export class index extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <View style={ui.container}>
        <View style={ui.header}>
          <Text style={ui.judul}> Registration Completed! </Text>
        </View>
        <View style={ui.successInput}>
          <Image
            style={{width: 150, height: 150}}
            source={require('../../../assets/logo.png')}
          />
          <Text style={ui.success}>
            We sent email verification to your email
          </Text>
        </View>
        <TouchableOpacity
          style={ui.createButton}
          onPress={() => {
            this.props.navigation.navigate('Login', {
              nama: this.state.isiNama,
            });
          }}>
          <Text style={ui.textCreate}>Back to Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default index;
