import React, {Component} from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  Button,
} from 'react-native';
import {showMessage, hideMessage} from 'react-native-flash-message';
import ui from '../../login';
import axios from 'axios';

export class index extends Component {
  constructor() {
    super();
    this.state = {
      isiEmail: '',
      isiPassword: '',
      isiNama: '',
      regexEmail: '',
      regexPassword: '',
    };
  }

  handleSubmit = async () => {
    if (this.state.regexEmail === true && this.state.regexPassword === true) {
      try {
        console.log(this.state.isiEmail);
        console.log(this.state.isiPassword);
        console.log(this.state.isiNama);
        const hasil = await axios.post(
          'http://code.aldipee.com/api/v1/auth/register',
          {
            email: this.state.isiEmail,
            password: this.state.isiPassword,
            name: this.state.isiNama,
          },
        );
        showMessage({
          message: `Registrasi Berhasil! Instruksi dikirim ke email ${this.state.isiEmail}`,
          type: 'success',
        });
        console.log(hasil);
        this.props.navigation.navigate('Success');
      } catch (error) {
        console.log(error);
      }
    } else {
      console.log('not submitted');
      showMessage({
        message:
          'Password harus terdiri dari 8 karakter atau lebih (huruf besar, kecil serta angka)',
        type: 'danger',
        duration: 3000,
      });
    }
  };

  handleChangeEmail = event => {
    this.setState({
      isiEmail: event,
    });
    const regex =
      /^([a-zA-Z0-9-\.\_\+]+@([a-zA-Z0-9-](([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})))$/;
    regex.test(event)
      ? this.setState({regexEmail: true})
      : this.setState({regexEmail: false});
  };

  handleChangePassword = event => {
    this.setState({
      isiPassword: event,
    });
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
    regex.test(event)
      ? this.setState({regexPassword: true})
      : this.setState({regexPassword: false});
  };

  handleChangeNama = event => {
    this.setState({
      isiNama: event,
    });
  };

  render() {
    return (
      <View style={ui.container}>
        <View style={ui.header}>
          <Image
            style={{width: 150, height: 150}}
            source={require('../../../assets/logo.png')}
          />
        </View>
        <View style={ui.input}>
          <Text style={ui.judul}> Create Account </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={ui.textInput}>Email:</Text>
            {this.state.regexEmail ? (
              <Text style={ui.textInputValid}>Valid</Text>
            ) : (
              <Text style={ui.textInputInvalid}>Invalid</Text>
            )}
          </View>
          <TextInput
            placeholder="Isi dengan Email kamu.."
            value={this.state.isiEmail}
            onChangeText={this.handleChangeEmail}
            style={ui.kotakInputEmail}
          />
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={ui.textInput}>Password:</Text>
            {this.state.regexPassword ? (
              <Text style={ui.textInputValid}>Valid</Text>
            ) : (
              <Text style={ui.textInputInvalid}>Invalid</Text>
            )}
          </View>
          <TextInput
            secureTextEntry={true}
            placeholder="Buat Password kamu.."
            value={this.state.isiPassword}
            onChangeText={this.handleChangePassword}
            style={ui.kotakInputEmail}
          />
          <Text style={ui.textInput}>Nama Lengkap:</Text>
          <TextInput
            placeholder="Isi nama kamu.."
            value={this.state.isiNama}
            onChangeText={this.handleChangeNama}
            style={ui.kotakInput}
          />
          <TouchableOpacity style={ui.createButton} onPress={this.handleSubmit}>
            <Text style={ui.textCreate}>Create Account</Text>
          </TouchableOpacity>
          <View
            style={{
              marginTop: 5,
              flex: 1,
              flexDirection: 'row',
            }}>
            <Text style={ui.textInput}>Already have an account?</Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Login', {
                  nama: this.state.isiNama,
                });
              }}>
              <Text style={ui.login}> Log in</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default index;
