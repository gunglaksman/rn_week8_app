import 'react-native-gesture-handler';
import * as React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from './pages/home';
import Detail from './pages/detail';
import Login from './pages/login';
import Signup from './pages/signup';
import Success from './pages/signup/success';
import Favorite from './pages/favorite/';
import Search from './pages/search/';
import Test from './pages/test/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHome, faHeart, faSearch} from '@fortawesome/free-solid-svg-icons';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
function InitialStackHome() {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#FFFF',
        style: {
          borderRadius: 0,
          paddingTop: 3,
          paddingBottom: 4,
          height: 60,
          backgroundColor: '#12172F',
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} size={size} icon={faHome} />
          ),
        }}
      />
      <Tab.Screen
        name="Favorite"
        component={Favorite}
        options={{
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} size={size} icon={faHeart} />
          ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} size={size} icon={faSearch} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

// export default function App() {
//   return (
//     <NavigationContainer>
//       <Tab.Navigator>
//         <Tab.Screen name="Home" component={InitialStackHome} />
//         <Tab.Screen name="Favorite" component={Favorite} />
//         <Tab.Screen name="Search" component={Search} />
//       </Tab.Navigator>
//     </NavigationContainer>
//   );
// }

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Signup"
          component={Signup}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Test"
          component={Test}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Success"
          component={Success}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Home"
          component={InitialStackHome}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DetailMovie"
          component={Detail}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
