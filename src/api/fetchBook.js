import React from 'react';
import {View, Text} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const fetchBook = async () => {
  try {
    const token = await AsyncStorage.getItem('token');
    const hasil = await axios.get('http://code.aldipee.com/api/v1/books', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    console.log(hasil);
  } catch (error) {
    console.log(error);
  }
};

export default fetchBook;
