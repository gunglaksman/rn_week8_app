import {ADD_BOOK, FAV_BOOK} from '../actions/types';

const initialState = {
  bookList: [],
  favList: [],
};

const bookReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_BOOK:
      return {...state, bookList: action.data};
    case FAV_BOOK:
      return {...state, favList: action.data};
    default:
      return state;
  }
};

export default bookReducer;
