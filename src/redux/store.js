import {createStore, combineReducers} from 'redux';
import bookReducer from './reducers/bookReducer';

const rootReducer = combineReducers({
  bookReducer: bookReducer,
});

const configureStore = () => createStore(rootReducer);

export default configureStore;
