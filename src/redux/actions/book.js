import {ADD_BOOK, FAV_BOOK} from './types';

export const addBook = book => ({
  type: ADD_BOOK,
  data: book,
});

export const favBook = book => ({
  type: FAV_BOOK,
  data: book,
});
