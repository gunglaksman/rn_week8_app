import React from 'react';

import Application from './src';
import FlashMessage from 'react-native-flash-message';

export default class App extends React.Component {
  render() {
    return (
      <>
        <Application />
        <FlashMessage position="top" />
      </>
    );
  }
}
